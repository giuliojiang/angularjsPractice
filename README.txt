This repo contains exercises I do following online tutorials
about AngularJS.
I am not the creator of these exercises. All credits go to the original
tutorials websites and authors.